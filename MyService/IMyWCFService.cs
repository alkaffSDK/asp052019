﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MyService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMyWCFService" in both code and config file together.
    [ServiceContract]
    public interface IMyWCFService
    {
        [OperationContract]
        string SayHello();

        [OperationContract]
        string Hello(string name);

        [OperationContract]
        int Add(int a, int b);
    }
}
