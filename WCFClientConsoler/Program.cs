﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCFClientConsoler
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = null;
            Console.Write("Name : ");
            name = Console.ReadLine();
            using (ServiceReference1.MyWCFServiceClient c = new ServiceReference1.MyWCFServiceClient("NetTcpBinding_IMyWCFService"))
            {
                Console.WriteLine("NetTcp:"+ c.Hello(name));
            }

            using (ServiceReference1.MyWCFServiceClient c = new ServiceReference1.MyWCFServiceClient("BasicHttpBinding_IMyWCFService"))
            {
                Console.WriteLine("Http:" + c.Hello(name));
            }
            Console.ReadKey();
        }
    }
}
