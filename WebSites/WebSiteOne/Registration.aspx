﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Title Page-->
    <title>Registration form</title>
    <!-- Required meta tags-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Colorlib Templates" />
    <meta name="author" content="Colorlib" />
    <meta name="keywords" content="Colorlib Templates" />


    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all" />
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all" />
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet" />

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all" />
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all" />

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
            <div class="wrapper wrapper--w680">
                <div class="card card-1">
                    <div class="card-heading"></div>
                    <div class="card-body">
                        <h2 class="title">Registration Info</h2>
                        <div>
                            <div class="input-group">
                                <asp:TextBox ID="TextBoxName" runat="server" CssClass="input--style-1" placeholder="NAME"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" ForeColor="Red" runat="server" ErrorMessage="Name is required." ControlToValidate="TextBoxName" Display="Dynamic">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorName" ForeColor="Red" ValidationExpression="^[A-Z][a-zA-Z ]*" runat="server" ErrorMessage="Invalid Name." ControlToValidate="TextBoxName" Display="Dynamic">*</asp:RegularExpressionValidator>
                            </div>

                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBoxBirthDate" CssClass="input--style-1 js-datepicker" runat="server" placeholder="dd/mm/yyyy"></asp:TextBox>
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorBirthdate" ForeColor="Red" runat="server" ErrorMessage="Birthdate is required." ControlToValidate="TextBoxBirthDate" Display="Dynamic">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        
                                            <asp:DropDownList ID="DropDownListGender" runat="server" Width="100%">
                                                <asp:ListItem Text="GENDER" Value="-1" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Male" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Female" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Other" Value="2"></asp:ListItem>
                                            </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorGender" ForeColor="Red" runat="server" ErrorMessage="Gender is required." InitialValue="-1" ControlToValidate="DropDownListGender" Display="Dynamic">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBoxPassword" CssClass="input--style-1" runat="server" TextMode="Password" placeholder="Passowrd"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" ForeColor="Red" runat="server" ErrorMessage="Password is required." ControlToValidate="TextBoxPassword" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBoxRepassword" CssClass="input--style-1" runat="server" TextMode="Password" placeholder="Re-passowrd"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorRepassword" ForeColor="Red" runat="server" ErrorMessage="Re-password is required." ControlToValidate="TextBoxRepassword" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidatorPassword" runat="server" ErrorMessage="Passwords are not match." ControlToValidate="TextBoxRepassword" ForeColor="Red" ControlToCompare="TextBoxPassword" Text="*"></asp:CompareValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <div class="select--no-search">
                                            <asp:DropDownList ID="DropDownListCountry" runat="server" Width="100%" DataSourceID="SqlDataSource1" DataTextField="CountryName" DataValueField="CountryId" OnDataBound="DropDownListCountry_DataBound">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CountriesConnectionString %>" SelectCommand="SELECT [CountryId], [CountryName] FROM [Country]"></asp:SqlDataSource>
                                        </div>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCountry" ForeColor="Red" runat="server" ErrorMessage="Country is required." InitialValue="0" ControlToValidate="DropDownListCountry" Display="Dynamic">*</asp:RequiredFieldValidator>

                                    </div>

                                </div>

                                <div class="col-2">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBoxEmail" CssClass="input--style-1" placeholder="Email" TextMode="Email" runat="server" Width="100%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" ForeColor="Red" runat="server" ErrorMessage="Email is required." ControlToValidate="TextBoxEmail" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" ForeColor="Red" runat="server" ErrorMessage="Invalid email." ControlToValidate="TextBoxEmail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Text="*"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="p-t-20">
                            <div class="row row-space">
                                <div class="col-2">
                                    <asp:Button ID="Button1" CssClass="btn btn--radius btn--green" runat="server" Text="Submit" OnClick="Button1_Click" />

                                </div>
                                <div class="col-2">
                                    <asp:Button ID="Button2" CssClass="btn btn--radius" runat="server" Text="Home" ValidationGroup="home" />
                                </div>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
                                <asp:Label ID="LabelResult" runat="server" Text=""></asp:Label>
                                 </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Jquery JS-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!-- Vendor JS-->
        <script src="vendor/select2/select2.min.js"></script>
        <script src="vendor/datepicker/moment.min.js"></script>
        <script src="vendor/datepicker/daterangepicker.js"></script>

        <!-- Main JS-->
        <script src="js/global.js"></script>

    </form>
</body>
</html>
