﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Registration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

    }

    protected void DropDownListCountry_DataBound(object sender, EventArgs e)
    {
        DropDownListCountry.Items.Insert(0, new ListItem("--Country---","0"));
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if(IsValid)
        {
            string[] name = TextBoxName.Text.Split();
            string fname = name[0];
            string lname = TextBoxName.Text.Substring(fname.Length);
           
            string password = TextBoxPassword.Text;
            string email = TextBoxEmail.Text;
           
            DateTime bdate = DateTime.ParseExact(TextBoxBirthDate.Text,"dd/MM/yyyy",null );
            int gender;
            int.TryParse(DropDownListGender.SelectedValue, out gender) ;
            int CountryID;
            int.TryParse(DropDownListCountry.SelectedValue, out CountryID);

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CountriesConnectionString"].ConnectionString))
            using (SqlCommand cmd = new SqlCommand("AddUser", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("FName", fname);
                cmd.Parameters.AddWithValue("LName", lname);
                cmd.Parameters.AddWithValue("BirthDay", bdate);
                cmd.Parameters.AddWithValue("Gender", gender);
                cmd.Parameters.AddWithValue("Email", email);
                cmd.Parameters.AddWithValue("Phone", "0788");
                cmd.Parameters.AddWithValue("Password", password);
                cmd.Parameters.AddWithValue("CountryID", CountryID);
                SqlParameter par = new SqlParameter("result",SqlDbType.Int);
                par.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(par);
                
                con.Open();
                int result;
                cmd.ExecuteNonQuery();
                int.TryParse(par.Value.ToString(), out result);

                if (result > 0)
                {
                    Clear();
                    LabelResult.Text = "Used is added.";
                    LabelResult.ForeColor = System.Drawing.Color.Green;

                }
                   
                else {
                    LabelResult.Text = "Error while addeding user: "+ result;
                    LabelResult.ForeColor = System.Drawing.Color.Red;
                }



            }
        }
    }

    private void Clear()
    {
        TextBoxName.Text = "";
        TextBoxPassword.Text = "";
        TextBoxRepassword.Text = "";
        TextBoxBirthDate.Text = "";
        TextBoxEmail.Text = "";
        DropDownListGender.SelectedIndex = 0;
        DropDownListCountry.SelectedIndex = 0;
    }
}