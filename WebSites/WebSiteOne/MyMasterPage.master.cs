﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MyMasterPage : System.Web.UI.MasterPage
{

    public String LableText { get { return LabelName.Text; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(Session["un"] != null)
            {
                ShowLogout();
            }
            
        }
    }

    protected void LinkClicked(object sender, EventArgs e)
    {
        Server.Transfer("~/Default2.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Control cnt =  sender as Control;
        switch(cnt.ClientID)
        {
            case "ButtonLogin":
                string name = TextBoxUserName.Text;
                string pass = TextBoxPassword.Text;

                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(pass) && name.Equals(pass, StringComparison.CurrentCultureIgnoreCase))
                {
                    Session["un"] = name;
                    ShowLogout();
                }
                break;
            case "LinkButtonLogout":
                Session["un"] = null;
                Server.Transfer(Request.RawUrl);
                break;

        }
        
    }

    private void ShowLogout()
    {
        login.Visible = false;
        logout.Visible = true;
        LabelName.Text += Session["un"].ToString();
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        TextBox tb = ContentPlaceHolderBody.FindControl("TextBox1") as TextBox;
        if(tb != null)
        {
            TextBox1.Text = tb.Text;
        }else
        {
            TextBox1.Text = "No data";
        }
    }
}
