﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server" Text="ASP TextBox"></asp:TextBox><br />
        <input id="Text1" type="text"   value="HTML textBox" />
        <br />
        <asp:Button ID="Button1" runat="server" Text="ASP Button" OnClick="Button1_Click"  /><br /> 
        <input id="Button2" type="button" runat="server" value="html button" onclick="Button1_Click"  />&nbsp;
        <br />      


        <asp:TextBox ID="TextBox2" runat="server" Text="0"></asp:TextBox>
        <asp:Button ID="Button3" runat="server" Text="Count" OnClick="Button3_Click" />
        <input id="counter" type="text" value="0" visible="false"  runat="server"/>
        <br />
        <br />
        <asp:TextBox ID="TextBox3" runat="server" Height="5%" Width="20%"></asp:TextBox>
        <asp:Button ID="Button4" runat="server" Text="Add" Height="5%" OnClick="Button4_Click" Width="10%" />
         <br />
        <br />
        <asp:DropDownList ID="DropDownList1" runat="server" Height="5%" Width="30%">
          <%--  <asp:ListItem Text="--Please Select--" Value="-1"></asp:ListItem>--%>
        </asp:DropDownList>
         <br />
        <br />
        <asp:DropDownList ID="DropDownList2" runat="server" Height="5%" Width="30%" DataSourceID="SqlDataSource1" DataTextField="CountryName" DataValueField="CountryId" OnDataBound="DropDownList2_DataBound">
        </asp:DropDownList>
         <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CountriesConnectionString %>" SelectCommand="SELECT [CountryId], [CountryName] FROM [Country]"></asp:SqlDataSource>
         </div>
    </form>
</body>
</html>
