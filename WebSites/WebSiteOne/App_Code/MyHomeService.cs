﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for MyHomeService
/// </summary>
[WebService(Namespace = "http://sdkjordan.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.None)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class MyHomeService : WebService
{

    public MyHomeService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(MessageName = "Hello", CacheDuration = 3600  )]
    public string Hello()
    {
        return "Hello World";
    }

    [WebMethod(MessageName = "HelloName")]
    public string Hello(string name)
    {
        return "Hello " + name; 
    }

    [WebMethod (Description ="To add 2 integers", EnableSession = true)]
    public int Add(int a, int b)
    {
        Session["last_a"] = a;
        Session["last_b"] = b;
        return a+ b;
    }


    [WebMethod(Description = "Read file")]
    public string ReadFile(string name)
    {
        // TODO: read the file 
        return "";
    }




}
