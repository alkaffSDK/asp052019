﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for DatabaseManager
/// </summary>
public class DatabaseManager
{
    public DatabaseManager()
    {
        
    }

    public int SaveFile(string filename, string fileContentType, int contentSize, byte[] fileContent )
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["asp052019ConnectionString"].ConnectionString))
        using (SqlCommand cmd = new SqlCommand("INSERT INTO [FILES] values (@name, @type,@size,@content);",con))
        {
            cmd.Parameters.AddWithValue("@name", filename);
            cmd.Parameters.AddWithValue("@type", fileContentType);
            cmd.Parameters.AddWithValue("@size", contentSize);
            cmd.Parameters.AddWithValue("@content", fileContent);
            con.Open();
            return cmd.ExecuteNonQuery();
        }
    }


}