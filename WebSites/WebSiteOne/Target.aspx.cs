﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Target : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TextBox1.Text += "===========QUERY STRINGS===========\n";
            foreach (var key in Request.QueryString.AllKeys)
            {
                TextBox1.Text += string.Format("{0,-15} : {1}\n", key,Server.HtmlDecode(Request.QueryString[key]));
            }
            TextBox1.Text += "===========POSTED FORM===========\n";
            foreach (var key in Request.Form.AllKeys)
            {
                if(!key.StartsWith("__"))
                TextBox1.Text += string.Format("{0,-15} : {1}\n", key, Server.HtmlDecode(Request.Form[key]));
            }

            if(PreviousPage != null)
            {

                TextBox1.Text += "===========PUBLIC PROPRITIES===========\n";
                // if we add <%@ PreviousPageType  VirtualPath="~/Source.aspx"%> to the Target page
                //TextBox1.Text += string.Format("{0,-15} : {1}\n", "UserName", PreviousPage.UserName);
                //TextBox1.Text += string.Format("{0,-15} : {1}\n", "password", PreviousPage.Password);

                TextBox tb = PreviousPage.FindControl("TextBox1") as TextBox;
                if(tb != null)
                    TextBox1.Text += string.Format("{0,-15} : {1}\n", "UserName", tb.Text);

                tb = PreviousPage.FindControl("TextBoxPassword") as TextBox;
                if (tb != null)
                    TextBox1.Text += string.Format("{0,-15} : {1}\n", "Password", tb.Text);


            }


        }

    }
}