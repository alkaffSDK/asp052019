﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Source : System.Web.UI.Page
{
    public string UserName { get { return TextBox1.Text; } }
    public string Password { get { return TextBoxPassword.Text; } }


    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        
        if(sender is Button)
        {
            Button btn = (Button) sender;
            switch(btn.ClientID)
            {
                case "ButtonRedirectInternal":
                    Response.Redirect("~/Target.aspx?text="+ Server.HtmlEncode(TextBox1.Text) + "&pass="+Server.HtmlEncode(TextBoxPassword.Text));
                    break;
                case "ButtonRedirectExternal":
                    Response.Redirect("http://sdkjordan.com/");
                    break;
                case "ButtonServerTransfer":
                    Server.Transfer("~/Target.aspx?text=" + Server.HtmlEncode(TextBox1.Text) + "&pass=" + Server.HtmlEncode(TextBoxPassword.Text));
                    break;
                case "ButtonServerExecute":
                    Server.Execute("~/Target.aspx?text=" + Server.HtmlEncode(TextBox1.Text) + "&pass=" + Server.HtmlEncode(TextBoxPassword.Text));
                    break;
                case "ButtonWindowOpen":
                    //Response.Write("<h1>Welcome</h1>");
                    Response.Write("<script>");
                    Response.Write(" window.open('Target.aspx', '_blank');");
                    Response.Write("</script>");
                    Response.End();
                    break;


            }
        }
       
    }
}