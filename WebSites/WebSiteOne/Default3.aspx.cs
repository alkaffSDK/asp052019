﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["un"] == null)
                Server.Transfer("~/Main.aspx");
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        // Option 1: work if the data needed is stored in Session 
        // TextBox1.Text = Session["un"].ToString();


        // Option 2:
        //Label l = Master.FindControl("LabelName") as Label;
        //TextBox1.Text = l.Text;

        // Option 3:
        //TextBox1.Text = ((MyMasterPage)Master).LableText;

        // Option 4: by adding <%@ MasterType VirtualPath="~/MyMasterPage.master"  %> to content page
      
        TextBox1.Text = Master.LableText;

    }
}