﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

public partial class FileUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            loadFiles();
    }

    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        if(FileUpload1.HasFiles)
        {
            foreach(var file in FileUpload1.PostedFiles)
            {
                file.SaveAs(Server.MapPath("~/files/") + file.FileName);
                saveToDB(file);
            }

            loadFiles(); 
        }
    }

    private void saveToDB(HttpPostedFile file)
    {
        int filesize = file.ContentLength;
        byte[] content = new byte[filesize];
        using (BinaryReader br = new BinaryReader(file.InputStream))
        {
            content = br.ReadBytes(file.ContentLength);
        }
       new DatabaseManager().SaveFile(file.FileName, file.ContentType, filesize, content);
    }

    private void loadFiles()
    {
        DirectoryInfo df = new DirectoryInfo(Server.MapPath("~/files/"));

        DataTable t = new DataTable();


        int id = 0;
        GridViewRow row;
        TableCell c1, c2, c3 ,c4;
        LinkButton link; 
        foreach (var file in df.GetFiles())
        {
             row = new GridViewRow(id + 1, id + 1, DataControlRowType.DataRow, DataControlRowState.Normal);
             c1 = new TableCell();
            c1.Text = id.ToString();
            row.Cells.Add(c1);


             c2 = new TableCell();
            c2.Text = file.Name;
            row.Cells.Add(c2);

            c3 = new TableCell();
            c3.Text = file.Length.ToString();
            row.Cells.Add(c3);

            c4 = new TableCell();
            link = new LinkButton();
            link.ID = "LinkDownlaod" + id;
            link.Text = file.Name;

            c4.Controls.Add(link);
            row.Cells.Add(c4);
            id++;
        }

        //GridView1.DataSource = t;
        GridView1.DataBind();
    }

    protected void DropDownList1_DataBound(object sender, EventArgs e)
    {
        DropDownList1.Items.Insert(0, "--Files--");
    }

    private void DownloadFile(FileInfo file)
    {
        Response.Clear();
        Response.ClearHeaders();
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
        Response.AddHeader("Content-Length", file.Length.ToString());
        Response.ContentType = "text/plain";
        Response.Flush();
        Response.TransmitFile(file.FullName);
        Response.End();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        FileInfo file = new FileInfo(Server.MapPath("~/files/")+ DropDownList1.SelectedItem);
        DownloadFile(file);
        DropDownList1.SelectedIndex = -1;
    }
}