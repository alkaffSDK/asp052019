﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ServiceUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        using (HomeServiceReference.MyHomeServiceSoapClient cleint 
            = new HomeServiceReference.MyHomeServiceSoapClient())
        {
            int a = 0, b = 0;
            int.TryParse(TextBox1.Text, out a);
            int.TryParse(TextBox2.Text, out b);
            Label1.Text = cleint.Add(a, b).ToString();
        }
    }
}