﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Source.aspx.cs" Inherits="Source" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            float: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table align="left" class="auto-style1">
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox1" runat="server" Width="100%"></asp:TextBox>
                    </td>
                    <td><asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://sdkjordan.com/">HyperLink External</asp:HyperLink>
                    </td>
                    <td>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Target.aspx">HyperLink Internal</asp:HyperLink>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="http://sdkjordan.com/" Target="_blank">HyperLink External in new Tab</asp:HyperLink>
                    </td>
                    <td>
                        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Target.aspx" Target="_blank">HyperLink Internal in new Tab</asp:HyperLink>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ButtonRedirectInternal" runat="server" Text="Redirect Internal" OnClick="Button1_Click" />
                    </td>
                    <td>
                        <asp:Button ID="ButtonRedirectExternal" runat="server" Text="Redirect External" OnClick="Button1_Click" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ButtonServerTransfer" runat="server" Text="Server Transfar" OnClick="Button1_Click" />
                    </td>
                    <td>
                        <asp:Button ID="ButtonServerExecute" runat="server" Text="Server Execute" OnClick="Button1_Click" /></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ButtonPostBack" runat="server" PostBackUrl="~/Target.aspx" Text="Cross-Post Back" OnClick="Button1_Click" /></td>
                    <td><asp:Button ID="ButtonWindowOpen" runat="server"  Text="Window Open" OnClick="Button1_Click" /></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <br />


            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;


         <br />
            &nbsp;&nbsp;&nbsp;&nbsp;
    
        <br />

        </div>
    </form>
</body>
</html>
