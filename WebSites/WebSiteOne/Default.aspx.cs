﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if(! IsPostBack)
        {
            if (Session["un"] == null)
                    Server.Transfer("~/Main.aspx");
           
            DropDownList1.Items.Insert(0, "--Please select--");
        }

    }

 
    protected void Button1_Click(object sender, EventArgs e)
    {
        //TextBox1.Text = "";
        //Text1.Value = "";
    }
        
    protected void Button3_Click(object sender, EventArgs e)
    {
        int c = 0;
        int.TryParse(counter.Value, out c);
        TextBox2.Text = (++c).ToString();
        counter.Value = c.ToString(); 
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        if( ! string.IsNullOrEmpty(TextBox3.Text) )
        {
            string text = 
                string.Format("{0,5}){1}",
                DropDownList1.Items.Count, TextBox3.Text);
            DropDownList1.Items.Add(text);
            TextBox3.Text = "";
        }
    }

    protected void DropDownList2_DataBound(object sender, EventArgs e)
    {
        DropDownList2.Items.Insert(0, "--Country--");

    }
}