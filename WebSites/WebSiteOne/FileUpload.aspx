﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileUpload.aspx.cs" Inherits="FileUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FileUpload ID="FileUpload1" runat="server" Width="30%" AllowMultiple="True" />
        <br />
        <asp:Button ID="ButtonSave" runat="server" Text="Save" Width="30%" OnClick="ButtonSave_Click"  />
    </div>
        <asp:GridView ID="GridView1" runat="server">
            <Columns>
                <asp:BoundField HeaderText="ID" />
                <asp:BoundField HeaderText="File Name" />
                <asp:BoundField HeaderText="File size (bytes)" />
                <asp:HyperLinkField Text="Donwload link" />
            </Columns>
        </asp:GridView>
        <br />
        Files from Database<br />
        <asp:DropDownList ID="DropDownList1" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource1" DataTextField="FileName" DataValueField="ID" Width="30%" OnDataBound="DropDownList1_DataBound" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:asp052019ConnectionString %>" SelectCommand="SELECT [ID], [FileName] FROM [Files]"></asp:SqlDataSource>
    </form>
</body>
</html>
