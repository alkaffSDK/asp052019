﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MultiView.aspx.cs" Inherits="MultiView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="View1" runat="server">
                <asp:TextBox ID="TextBoxName" runat="server" PlaceHolder="Name"></asp:TextBox><br />
                <asp:TextBox ID="TextBoxEmail" runat="server" PlaceHolder="Email"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="Next" OnClick="Button2_Click"/>
            </asp:View>
             <asp:View ID="View2" runat="server">
                 <asp:RadioButtonList ID="RadioButtonListGender" runat="server">
                     <asp:ListItem>Female</asp:ListItem>
                     <asp:ListItem>Male</asp:ListItem>

                 </asp:RadioButtonList>
                <asp:TextBox ID="TextBox2" runat="server" PlaceHolder="Email"></asp:TextBox>
                  <br />
                  <asp:Button ID="Button2" runat="server" Text="Next" OnClick="Button2_Click" />
             </asp:View>
            <asp:View ID="View3" runat="server">
                <asp:TextBox ID="TextBox3" runat="server" PlaceHolder="Name"></asp:TextBox><br />
                <asp:TextBox ID="TextBox4" runat="server" PlaceHolder="Email"></asp:TextBox>
                 <asp:Button ID="ButtonFinish" runat="server" Text="Finish" />
            </asp:View>
        </asp:MultiView>
    </div>
    </form>
</body>
</html>
