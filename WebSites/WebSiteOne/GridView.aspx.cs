﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GridView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        LabelData.Text =string.Format("{0,20} : {1}", GridView1.SelectedRow.Cells[2].Text, GridView1.SelectedRow.Cells[7].Text);
    }

    protected void LinkButtonInsert_Click(object sender, EventArgs e)
    {
        string fname = null, lname = null, bdate = null;
        TextBox tb = GridView1.FooterRow.FindControl("TextBoxFName") as TextBox;
        if (tb != null)
            fname = tb.Text;

         tb = GridView1.FooterRow.FindControl("TextBoxLName") as TextBox;
        if (tb != null)
            lname = tb.Text;

        tb = GridView1.FooterRow.FindControl("TextBoxBirthDate") as TextBox;
        if (tb != null)
            bdate = tb.Text;

        SqlDataSource1.InsertParameters["FirstName"].DefaultValue = fname;


        SqlDataSource1.Insert();
        GridView1.DataBind();

    }
}