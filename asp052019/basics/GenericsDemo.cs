﻿using OOPs;

namespace asp052019.basics
{

    class Pair
    {
        // key
        private object Key;

        public void SetKey(object k)
        {
            Key = k;
        }

        public object GetKey() { return Key; }

        // data 
        object Data;
        public void SetData(object d)
        {
            Data = d;
        }

        public object GetData() { return Data; }

    }

    class Pair<K,D> {
        // key
        private K Key;

        public void SetKey(K k)
        {
            Key = k;
        }

        public K GetKey() { return Key; }

        // data 
        D Data;
        public void SetData(D d)
        {
            Data = d;
        }

        public D GetData() { return Data; }
    }
    class GenericsDemo
    {

        public bool Equales(int a, int b)
        {
            return a == b;
        }
        public bool Equales(double a, double b)
        {
            return a == b;
        }

        public static bool Equals<T>(T a, T b)
        {
            return a.Equals(b);
        }
        static void Main(string[] args)
        {
            Pair p = new Pair();
            p.SetKey(1);
            p.SetData("String");

            Pair<int, string> p2 = new Pair<int, string>();
            p2.SetKey(1);
            p2.SetData("String");

       

            Pair p1 = new Pair();
            p1.SetKey("12");
            p1.SetData(new Person());

            Pair<string,Person> p3 = new Pair<string, Person>();
            p3.SetKey("12");
            p3.SetData(new Person());


            System.Console.WriteLine(Equals<Pair>(p,p1));


        }

    }
}
