﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp052019
{
    class Convertion
    {


        static void Main(string[] args)
        {
            int a = 10;     // 4 bytes 
            long l = 10;    // 8 byte 

            a = (int) l;          // explicit conversion
            a = (int) 15.3;
            a = Convert.ToInt32(l);

      
            //  a = (int) "15";


            a = Convert.ToInt32("15r");         // Error 
            a = int.Parse("15r");               // Error
            bool result1 = int.TryParse("15r", out a);         // No error and 'a' will be zero and result1 will be false
            bool result2 = int.TryParse("0", out a);          // 'a' will be zero and result2 will be true


            decimal d;
            d = Convert.ToDecimal("15.215");
            d = decimal.Parse("15.215");
            decimal.TryParse("15.215", out d);

            Console.Write("D:");
            decimal.TryParse(Console.ReadLine(), out d);
            Console.WriteLine("D value is :"+d);
            a = (int)d;
            Console.WriteLine("A value is :" + a);
            decimal b = d - a;
            Console.WriteLine("B value is :" + b);
            l = a;

            Console.ReadKey();
        }
    }
}
