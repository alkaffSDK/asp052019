﻿using System;

namespace asp052019
{
    class Loops
    {
        static void Main(string[] args)
        {
            //int d = 0;
            //for (Console.WriteLine("A"), Console.WriteLine("A1"), d = 0; ; Console.WriteLine("C"))
            //    Console.WriteLine();
          
            //int x = 0;
            //for(int r = 2; ;)
            //    Console.WriteLine();

            // while 
            short a = 10;
            while (a > 0)
            {
                Console.WriteLine(a);
                a++;
            }

            // IL , 32767 , 32777 , 32757, 32757 
            Console.WriteLine("Final A :"+a );      // 32767   -32768

            // for loop 
            int c =0  , d = 3;      // c = 9 
            // for ([value] ;[boolean_expression];[])   
            for (Console.WriteLine("A"); c++ < 10; Console.WriteLine("C"),c++)
            {
                Console.WriteLine("Statement or block, c :"+c);
            }

            // Do while 
            a = 10;
            do
            {
                if (a-- % 3 == 0)
                    continue;
             Console.WriteLine(a);

            }
            while (a>0);

            Console.ReadKey();

            do
                ;
            while (true);


        }
    }
}
