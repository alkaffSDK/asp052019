﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp052019
{
    
    class MethodsDemoClass
    {
        // Method or function : named block of code with return data type and optional input parameters 
        // All methods in C# must be written inside a class 
        // Syntax to define a method
        //      [specifier] [modifier] <return_data_type> <method_name> (<parameter_list>) { <body>}

        // Syntax to call a method
        //  [<Class_name>|<variable_name>.]methodName(<argument_list>);



        // method must be definded  inside a class

         void Method()
        {
            StaticMethod();
            return;     // return statement stops the method execution
        }

        int Addition()
        {

            int x = 10;
           if(x == 2)
            return 0;
           else
            return 10;   

           // All execution paths must end with return 
        }

        int Addition(int x)
        {
            if (x == 2)
                return 0;
            else
                return 10;
        }

        int Addition(int x, int y)
        {
            if (x == y)
                return 0;
            else
                return 10;
        }

        void ChangeMe(int x)        // x = 20 
        {
            x++;
            Console.WriteLine("ChangeMe() X : " + x);
        }

        void ChangeMe(ref int x)        // x = 20 
        {
            x++;
            Console.WriteLine("ChangeMe() X : " + x);
        }

        void ChangeMe( int[] x)        // x = 20 
        {
            Console.Write("{");
            for(int i=0;i<x.Length;i++)
            {
                x[i]++;
                Console.Write(x[i]+ ", ");
            }
            Console.Write("\b\b}\n");
        }

        class Holder
        {
            public int value;
        }
        void ChangeMe(Holder x)        // x = 20 
        {
            x.value++;
            Console.WriteLine("ChangeMe()  x.value : " + x.value);

        }

        static string Print(int[] x)       
        {
            StringBuilder build = new StringBuilder("{");

            for (int i = 0; i < x.Length; i++)
            {
                build.Append(x[i]);
                if (i != x.Length - 1)
                    build.Append(", ");
                else
                    build.Append("}");
            }

            return build.ToString();
        }
        static void StaticMethod()
        {

        }





        int nonStaticVariable;
        static int StaticVariable = 10;
        static void Main(string[] args)
        {
            StaticMethod();     // just from static method inside the class 

            MethodsDemoClass.StaticMethod();


            MethodsDemoClass obj = new MethodsDemoClass();

            int d = 20;
            Console.WriteLine("Before D :"+d);  // 20
            obj.ChangeMe(d);                    // 21
            Console.WriteLine("After D :" + d); // 20

            Console.WriteLine("---------------");
            Console.WriteLine("Before D :" + d);    // 20
            obj.ChangeMe(ref d);                    // 21
            Console.WriteLine("After D :" + d);     // 21

            Console.WriteLine("---------------");

            int[] arr = { 10, 8, 6, 96 };
            Console.WriteLine( Print(arr) );                // { 10, 8, 6, 96 };
            obj.ChangeMe(arr);          // { 11, 9, 7, 97 };
            Console.WriteLine(Print(arr));                   // { 11, 9, 7, 97 };

            Console.WriteLine("---------------");

            Holder h1 = new Holder();
            h1.value = 100;
            Console.WriteLine("Before  h1.value :" + h1.value);     // 100
            obj.ChangeMe(h1);                                       // 101
            Console.WriteLine("After  h1.value :" + h1.value);      // 101

            MethodsDemoClass var = new MethodsDemoClass();
           
            int result = var.Dif(1,5);

            Console.WriteLine("Result:"+result);

            // nonStaticVariable = 10;
            StaticVariable  = 20;



            Console.ReadKey();
        }

        void name(int v, int m){    // v = 1 , m = 5 
            nonStaticVariable = 10;
            StaticVariable = 30;
           
            return; // return ends the method execution 

            Console.WriteLine("Unreachable code ");
        }

        int Dif(int a, int b)  // a = 1 , b = 5
        {
            if (a > b)
                return a - b;
            //else
                return b - a;
        }

        
    }
}
