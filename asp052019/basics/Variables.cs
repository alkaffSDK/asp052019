﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp052019
{
     
    /// [] Optional
    /// <> Mendatory
    /// * : repeated zero or more
    /// + : repeated one or more
    /// | : OR
    class Variables
    {
      
        // Variables: is a name of a reference for a memory location
        // Syntax :[specifier] [modifier] <data_type> <variable_id> [=<expression>][,<variable_id> [=<expression>]]*;

        static void Main(string[] args)
        {

            int a = int.Parse(Console.ReadLine());

            Console.WriteLine("You entered :"+a);

            a = 10 + 25;
            int  m = 125, e;
            int x = 10;
            int c = 125487;


            bool boolean = true;


            char ch = 'A';
            Console.WriteLine("Ch: " + ch);     // A
            ch = '\u0041';                  // Unicode 
            Console.WriteLine("Ch: " + ch);     // A
            ch = (char) 65;
            Console.WriteLine("Ch: " + ch);     // A
            ch = (char)0x41;
            Console.WriteLine("Ch: " + ch);     // A


            // integers : byte, sbyte, short, long , int ,  , uint ,ushort , ulong

            byte by = 255;      // 0-255
            sbyte sby = 127;       // -128 to 127


            short shor = 32767;  // -32768 to 32767
            ushort ushot = 65535;        // 0- 65,535

            long l = 9223372036854775807;

            


            uint ui = 10;           // [U]
            Console.WriteLine(10U);
            long i = 10L;           // [L]
            Console.WriteLine(10L);
            ulong ul = 10lu;        // [LU|UL|U|L]


            c = 10;
            Console.WriteLine("C : "+c);        //10
            c = 0x10;
            Console.WriteLine("C : " + c);      // 16

          
            Console.WriteLine(10+ 5+ 3 );               // 18
            Console.WriteLine("10" + 5 + 3);            // 1053
            Console.WriteLine("10" + (5 + 3));          // 108
            Console.WriteLine(10 + "5" + 3);            // 1053 
            Console.WriteLine(10 + 5 + "3");            // 153
            Console.WriteLine(10 + '5' + 3);            // 66
            Console.WriteLine(10 + 5 + '3');            // 66

            Console.WriteLine("A :" + 3 + 'a');     // A :3a
            Console.WriteLine("A :" + (3 + "a"));     // A :3a
            Console.WriteLine("A :" + (3 + 'a'));   // A :100

            float f = 14511.123456789101112131415f;            // F
            double d = 14511.123456789101112131415;            // [D]
            decimal dec = 14511.123456789101112131415M;         // M

            Console.WriteLine("F: " + f);
            Console.WriteLine("D: " + d);
            Console.WriteLine("Dec: " + dec);


            //// Type conversion
            c = 1524;
            short sh =(short) c;
            Console.WriteLine("sh: " + sh);

            c = 1548752;
            sh = (short)c;
            Console.WriteLine("sh: " + sh);
            int re = sh;
            Console.WriteLine("re: " + re);

            x = (int)f;
            Console.WriteLine("x: " + x);

            x = Convert.ToInt32(boolean);
            
            x = Convert.ToInt32(f);     // I need to convert f to int

            Console.WriteLine(Convert.ToBoolean("false"));      // false
            Console.WriteLine(Convert.ToBoolean(false));        // false
            Console.WriteLine(Convert.ToBoolean(15));           // true



            string s = x.ToString();
            s = Convert.ToString(x);




            Console.ReadKey();

        }
    }
}
