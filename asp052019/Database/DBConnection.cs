﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace asp052019.Database
{
    class DBConnection
    {

        static void CheckUserLoginBadWay()
        {
            string connectionString = @"Integrated Security = SSPI; Persist Security Info = False; Initial Catalog = asp052019; Data Source =.\SQLSERVER";
            string command = "SELECT * FROM Users Where Name = \'{0}\' AND Password = \'{1}\';";
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand())
            {
                string name, pass;
                Console.Write("Your name:");
                name = Console.ReadLine().Trim();
                Console.Write("Your password:");
                pass = Console.ReadLine().Trim();

                cmd.Connection = con;
                cmd.CommandText = string.Format(command, name, pass);

                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                PrintData(reader);
            }
        }

        static void CheckUserLogin()
        {
            string connectionString = @"Integrated Security = SSPI; Persist Security Info = False; Initial Catalog = asp052019; Data Source =.\SQLSERVER";
            string command = "SELECT ID , Name as [User Name], Password as [Pass] FROM Users Where Name = @a AND Password = @b;";
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand())
            {
                string name, pass;
                Console.Write("Your name:");
                name = Console.ReadLine().Trim();
                Console.Write("Your password:");
                pass = Console.ReadLine().Trim();

                cmd.Connection = con;
                cmd.CommandText = command;

                cmd.Parameters.AddWithValue("@a", name);
                cmd.Parameters.AddWithValue("@b", pass);

                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    PrintData(reader);
                }


            }
        }
        static void Main(string[] args)
        {

            CheckUserLogin();

            /*
            // 1) Define connection String 
            String connectionString = @"Server=.\SQLSERVER;Database=asp052019;User Id=sa;Password=123;";
            String connectionString1 = @"Server=.\SQLSERVER;Database=asp052019; Trusted_Connection = True";
            String connectionString2 = @"Integrated Security = SSPI; Persist Security Info = False; Initial Catalog = asp052019; Data Source =.\SQLSERVER";
            // 2) create connection object 
            // 3) create command object 
            using (SqlConnection con = new SqlConnection(connectionString2))
            using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM Users;",con))
            {

                // 4) open the connection

                con.Open();

                Console.WriteLine("The connection state is {0} @ {1}", con.State, DateTime.Now);

                // 5) execute the command (for scalar ) 
                int counter;
                int.TryParse(cmd.ExecuteScalar().ToString(),out counter);
                Console.WriteLine("The number of users are :"+counter);


                // we need to change the command 

               // cmd.CommandText = "INSERT INTO Phones (Phone,UserID) values ('0788554554',3 )";

                //int numRos =  cmd.ExecuteNonQuery();
                //Console.WriteLine("The inserted rows are : "+ numRos);


                // for query commands 
                cmd.CommandText = "SELECT * FROM Users ;";

                SqlDataReader reader = cmd.ExecuteReader();

                PrintData(reader);
                reader.Close();


                // stored prosedure 
                Console.WriteLine("=========stored prosedure =============");

                cmd.CommandText = "GetUsersInfo";
                cmd.CommandType = CommandType.StoredProcedure;
                reader = cmd.ExecuteReader();
                PrintData(reader);
                reader.Close();

                Console.WriteLine("=========stored prosedure with parameter =============");

                cmd.CommandText = "GetUserInfoByID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("ID",1) ;
                PrintData(cmd.ExecuteReader());


            }       // connection will be disposed automaticlly 

    */
            Console.ReadKey();

        }

        private static void PrintData(SqlDataReader reader)
        {
            if (reader.HasRows)
            {
                int colCount = reader.FieldCount;
                for (int i = 0; i < colCount; i++)
                {

                    Console.Write("{0,-15}|", reader.GetName(i));
                }
                Console.WriteLine("\n-------------------------------------------------------------");

                while (reader.Read())
                {
                    for (int i = 0; i < colCount; i++)
                    {
                        Console.Write("{0,-15}|", reader[i]);
                    }
                    Console.WriteLine();

                }
            }
            else
            {
                Console.WriteLine("No data found!!");
            }
        }
    }

   
}
