﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceUserApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            Console.Write("Name :");
            name = Console.ReadLine();
            int a = 0, b = 0;
            Console.Write("A :");
            int.TryParse(Console.ReadLine(), out a);

            Console.Write("B :");
            int.TryParse(Console.ReadLine(), out b);

            using (ServiceReference1.MyHomeServiceSoapClient c = new ServiceReference1.MyHomeServiceSoapClient())
            {
                Console.WriteLine(c.HelloWorld());
                Console.WriteLine(c.Hello(name));
                Console.WriteLine(c.Add(a, b));


            }
            Console.ReadKey();
        }
    }
}
