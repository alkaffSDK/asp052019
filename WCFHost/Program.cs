﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;


namespace WCFHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(MyService.MyWCFService)))
            {
                host.Open();
                Console.WriteLine("The service host is: {0} @ {1} ", host.State , DateTime.Now);
                do
                {
                    Console.WriteLine("Press ESC to stop the host.");
                } while (! Console.ReadKey().Key.Equals(ConsoleKey.Escape));

                
            }

            Console.WriteLine("The host was closed.");
            Console.ReadKey();
        }
    }
}
